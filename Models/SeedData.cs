﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ToDoApi.Data;
using System;
using System.Linq;


namespace ToDoApi.Models
{
    public class SeedData
    {
        public static void Seed(IServiceProvider serviceProvider)
        {
            using (var context = new TodoContext(
                serviceProvider.GetRequiredService<DbContextOptions<TodoContext>>()))
            {
                if (context.TodoItems.Any())
                {
                    return;
                }

                context.AddRange(
                    new TodoItem
                    {
                        Name = "suck some duck",
                        IsComplete = false,
                        Description = "sample"
                    },
                    new TodoItem
                    {
                        Name = "duck some suck",
                        IsComplete = false,
                        Description = "sample"
                    },
                    new TodoItem
                    {
                        Name = "watch new movie",
                        IsComplete = false,
                        Description = "sample"
                    },
                    new TodoItem
                    {
                        Name = "listen to new gachi remixes",
                        IsComplete = false,
                        Description = "sample"
                    },
                    new TodoItem
                    {
                        Name = "find a girls",
                        IsComplete = true,
                        Description = "sample"
                    }

                    );
                context.SaveChanges();
            }
        }
    }
}
