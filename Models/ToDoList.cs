﻿using System.Collections.Generic;

namespace ToDoApi.Models
{
    public class ToDoList
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<TodoItem> ItemsList { get; set; }
    }
}
